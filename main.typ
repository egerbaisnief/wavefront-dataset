#align(center)[
  #text(size: 2em)[#strong[_Wavefront_] --- Set of various shapes]
]
#set image(width: 10em)
#show image: it => align(center, rect(stroke: 1pt, it))

#pagebreak()
= Triangle
#image("triangle.png")
#table(columns: (1fr, 1fr, 1fr),
  [Normal], $T = {A, B, C}$, `trangle.obj`,
  [Colinear], $arrow(A B) = x arrow(A C)$, `trangle-colinear.obj`,
  [Collapsed 1], $A = B$, `triangle-collapsed1.obj`,
  [Collapsed 2], $A = B = C$, `triangle-collapsed2.obj`,
  [Isolated], $D in.not T$, `triangle-isolated.obj`,
)

#pagebreak()
= Pyramid
#image("pyramid.png")
#table(columns: (1fr, 1fr, 1fr),
  [Normal], $P = {A, B, C, D, E}$, `pyramid.obj`,
  [Colinear], $E = x arrow(A B) + y arrow(A C)$, `pyramid-colinear.obj`,
  [Collapse 1], $A = B$, `pyramid-collapse1.obj`,
  [Isolated], $F in.not P$, `pyramid-isolated.obj`,
)

#pagebreak()
= Prism
#image("prism.png")
#table(columns: (1fr, 1fr, 1fr),
  [Normal], $P = {A, B, C, D, E, F, G, H}$, `pyramid.obj`,
  [Flat], $A = E, B = F, C = G, D = H$, `pyramid-flat.obj`,
)
